<?php

namespace App\Http\Controllers;

class AdminHomeController extends Controller
{
    public function __invoke()
    {
        $title = 'Home';
        return view('home.admin', compact('title'));
    }
}
