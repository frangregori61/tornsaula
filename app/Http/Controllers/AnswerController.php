<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function index()
    {
    }

    public function create($module_id, $unit_id, $question_id)
    {
        $question = Question::findOrFail($question_id);
        $question_id = $question->id;
        $title = 'Answer';

        return view('answers.create', compact('question_id', 'title'));
    }

    public function store(Request $request)
    {
        $request['user_id'] = auth()->id();

        $validatedData = $request->validate([
            'question_id' => 'required|integer|exists:questions,id',
            'user_id' => 'required|integer|exists:users,id',
            'content' => 'required|string|max:800'
        ]);

        $answer = Answer::create($validatedData);

        /** si estoy como un teacher, tengo que redirigir a 2 rutas
         * Si entro desde el menu de pending
         * /module/${module_id}/unit/${unit_id}/questions
         * Si entro desde el propio módulo
         * /module/${module_id}/unit/${unit_id}/questions-pending-to-answer
         */
        return redirect()->route('questions.byModuleAndUnit', ['module_id' => $answer->question->module_id, 'unit_id' => $answer->question->unit_id]);
    }

    public function show($id)
    {
    }

    public function edit($module_id, $unit_id, $question_id, $answer_id)
    {
        $title = 'Edit Answer';
        
        return view('answers.edit', compact('question_id', 'answer_id', 'title'));
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'content' => 'required|string|max:800'
        ]);

        $answer = Answer::findOrFail($id);
        $answer->update($validatedData);

        /** si estoy como un teacher, tengo que redirigir a 2 rutas
         * Si entro desde el menu de pending
         * /module/${module_id}/unit/${unit_id}/questions
         * Si entro desde el propio módulo
         * /module/${module_id}/unit/${unit_id}/questions-pending-to-answer
         */
        return redirect()->route('questions.byModuleAndUnit', ['module_id' => $answer->question->module_id, 'unit_id' => $answer->question->unit_id]);
    }

    public function destroy($id)
    {
    }

}
