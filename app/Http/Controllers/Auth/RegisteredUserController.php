<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Module;
use App\Models\Question;
use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $roles = Role::all();
        $title = 'Users';

        return view('users.index', compact('users', 'roles', 'title'));
    }
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:' . User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(route('home.guest', absolute: false));
    }

    public function userStoreRole(Request $request, $id)
    {
        $request->validate(['role_id' => 'integer|required|exists:roles,id']);

        $user = User::find($id);
        $user->role_id = $request['role_id'];
        $user->update();

        return redirect()->route('users.index');
    }

    public function byModule($module_id)
    {
        $module = Module::findOrFail($module_id);
        $users = $module->users;
        $questions = [];

        foreach ($users as $user) {
            array_push($questions, count(Question::where('module_id', $module_id)->where('user_id', $user->id)->get()));
        }

        return view('users.byModule', compact('users', 'questions', 'module_id'));
    }
}
