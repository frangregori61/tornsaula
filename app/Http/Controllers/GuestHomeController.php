<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuestHomeController extends Controller
{
    public function __invoke()
    {
        $title = 'Home';
        return view('home.guest', compact('title'));
    }
}
