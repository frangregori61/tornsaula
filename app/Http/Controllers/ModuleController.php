<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\Question;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ModuleController extends Controller
{
    public function index()
    {
        $modules = Module::all();
        $title = 'Modules';
        return view('modules.index', compact('modules', 'title'));
    }

    public function create()
    {
        $title = 'New Module';

        return view('modules.create', compact('title'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:60',
            'initials' => 'required|string|max:9',
            'description' => 'required|string|max:120'
        ]);
        Module::create($validatedData);

        return redirect()->route('modules.index');
    }

    public function show($id)
    {

    }

    public function edit($module_id)
    {
        $title = 'Edit Module';
        return view('modules.edit', compact('module_id', 'title'));
    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {
        $module = Module::findOrFail($id);
        $module->delete();

        return redirect()->route('modules.index');
    }

    public function usersByModule($id)
    {
        $module = Module::findOrFail($id);
        $title = strtoupper($module->initials) . " users";
        $users = $module->users;
        $roles = Role::all();
        $module_id = $id;

        return view('modules.usersByModule', compact('users', 'title', 'roles', 'module_id'));
    }

    public function storeModulesOfUser(Request $request)
    {
        $user = Auth::user();
        $user->modules()->sync($request['modules']);
        return redirect()->route('profile.edit');
    }


    public function modulesWithQuestionsPendingToAnswer()
    {
        $allQuestionsWithoutAnswer = Question::doesntHave('answer')->get();

        $userModules = auth()->user()->modules->toArray();
        $userModulesId = array_column($userModules, 'id');

        $modulesIdWithoutDuplicatesModulesId = [];
        foreach ($allQuestionsWithoutAnswer as $question) {
            if (
                !in_array($question->module_id, $modulesIdWithoutDuplicatesModulesId) &&
                in_array($question->module_id, $userModulesId)
            ) {
                array_push($modulesIdWithoutDuplicatesModulesId, $question->module_id);
            }
        }

        $modules = [];
        foreach ($modulesIdWithoutDuplicatesModulesId as $module_id) {
            array_push($modules, Module::findOrFail($module_id));
        }

        $questionsWithoutAnswerPerModule = [];
        foreach ($modules as $module_id) {
            array_push($questionsWithoutAnswerPerModule, $allQuestionsWithoutAnswer->whereIn('module_id', $module_id)->toArray());
        }
        $questions = $questionsWithoutAnswerPerModule;

        return view('modules.modulesWithQuestionsPendingToAnswer', compact('modules', 'questions'));
    }

    public function byUser($user_id)
    {
        $user = User::findOrFail($user_id);

        $modules = $user->modules;

        $questions = [];
        foreach ($modules as $module) {
            array_push($questions, count(Question::where('user_id', $user->id)->where('module_id', $module->id)->get()));
        }
        $title = $user->name . "'s questions";

        return view('modules.byUserId', compact('modules', 'questions', 'user', 'title'));
    }
}
