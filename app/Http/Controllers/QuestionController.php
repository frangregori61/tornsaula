<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\Question;
use App\Models\Unit;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class QuestionController extends Controller
{
    public function index(Request $request)
    {
    }

    public function create($module_id, $unit_id)
    {
        $unit = Unit::findOrFail($unit_id);
        $module = Module::findOrFail($module_id);

        $unit_id = $unit->id;
        $module_id = $module->id;
        $title = 'New Question';

        return view('questions.create', compact('module_id', 'unit_id', 'title'));
    }

    public function store(Request $request)
    {
        $request['user_id'] = auth()->id();
        $validatedData = $request->validate([
            'user_id' => 'required|integer|exists:users,id',
            'module_id' => 'required|integer|exists:modules,id',
            'unit_id' => 'required|integer|exists:units,id',
            'title' => 'required|string|max:55',
            'description' => 'required|string|max:800',
            'photo' => 'nullable|image'
        ]);

        if ($request->has('photo')) {
            $path = Storage::disk('public')->putFile($request->file('photo'));
            $validatedData['photo'] = $path;
        }

        $question = Question::create($validatedData);

        return redirect()->route('questions.byModuleAndUnit', ['module_id' => $question->module_id, 'unit_id' => $question->unit_id]);
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        $question->delete();

        return redirect()->back();
    }

    public function myQuestions()
    {
        // hacer por modulos y temas, igual que lo de pending
        $questions = Question::where('user_id', auth()->id())->get();
        return view('questions.myQuestions', compact('questions'));
    }

    public function byModule($module_id)
    {
        $module = Module::findOrFail($module_id);
        $initials = strtoupper($module->initials);
        return view('questions.byModule', compact('initials', 'module_id'));
    }

    public function byModuleAndUnit($module_id, $unit_id)
    {

        $module = Module::findOrFail($module_id);
        $unit = Unit::findOrFail($unit_id);

        $questions = Question::where('unit_id', $unit_id)->get();
        $title = strtoupper($module->initials) . ' Unit ' . $unit->unit;

        return view('questions.byUnitAndModule', compact('questions', 'title', 'unit', 'module_id', 'unit_id'));
    }

    public function pendingToAnswerByModuleAndUnit($module_id, $unit_id)
    {
        $questions = Question::doesntHave('answer')->where('module_id', $module_id)->where('unit_id', $unit_id)->get();
        $module = Module::findOrFail($module_id);
        $unit = Unit::findOrFail($unit_id);
        $title = strtoupper($module->initials) . ' Unit ' . $unit->unit . ' Questions to Answer';

        return view('questions.byModuleAndUnitPendingToAnswer', compact('title', 'questions'));
    }

    public function myQuestionsByUnitAndModule($module_id, $unit_id)
    {
        $user_id = auth()->user()->id;
        $questions = Question::where('user_id', $user_id)->where('module_id', $module_id)->where('unit_id', $unit_id)->get();
        $unit = Unit::findOrFail($unit_id);
        $module = Module::findOrFail($module_id);
        $title = 'My Questions of ' . strtoupper($module->initials) . ' Unit ' . $unit->unit;

        return view('questions.myQuestionsByUnitAndModule', compact('title', 'questions', 'user_id', 'module_id', 'unit_id'));
    }

    public function byUserAndModule($user_id, $module_id)
    {
        $questions = Question::where('user_id', $user_id)->where('module_id', $module_id)->get();
        $user = User::findOrFail($user_id);
        $module = Module::findOrFail($module_id);
        $title = $user->name . "'s " . strtoupper($module->initials) . " questions";

        return view('questions.myQuestions', compact('questions', 'title', 'module'));
    }
}
