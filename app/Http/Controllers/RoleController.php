<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        $title = 'Roles';
        return view('roles.index', compact('roles', 'title'));
    }

    public function create()
    {
        $title = 'New Role';

        return view('roles.create', compact('title'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'type' => 'string|required|max:30'
        ]);

        $validatedData['type'] = strtoupper($validatedData['type']);

        $role = Role::create($validatedData);

        return redirect()->route('roles.index');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();
        return redirect()->route('roles.index');
    }

    public function usersByRole($id)
    {
        $role = Role::find($id);
        $type = $role->type;
        $roles = Role::all();
        $users = $role->users;
        $title = 'Users with ' . $type . ' role';
        return view('roles.usersByRole', compact('users', 'roles', 'type', 'title'));
    }

}
