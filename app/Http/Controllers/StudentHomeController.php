<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;

class StudentHomeController extends Controller
{
    public function __invoke()
    {
        $myQuestions = Question::where('user_id', auth()->user()->id)->get();

        $modulesId = [];
        $modules = [];
        foreach ($myQuestions as $question) {
            if (!in_array($question->module->id, $modulesId)) {
                array_push($modulesId, $question->module->id);
                array_push($modules, $question->module);
            }
        }

        $questions = [];
        foreach ($modules as $module) {
            array_push($questions, $myQuestions->where('module_id', $module->id));
        }

        $title = 'Home';
        return view('home.student', compact('title', 'modules', 'questions'));
    }
}
