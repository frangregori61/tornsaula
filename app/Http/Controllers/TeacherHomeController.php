<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\Question;

class TeacherHomeController extends Controller
{
    public function __invoke()
    {
        $title = 'Home';
        $allQuestionsWithoutAnswer = Question::doesntHave('answer')->get();

        $userModules = auth()->user()->modules->toArray();
        $userModulesId = array_column($userModules, 'id');

        $modulesIdWithoutDuplicatesModulesId = [];
        foreach ($allQuestionsWithoutAnswer as $question) {
            if (
                !in_array($question->module_id, $modulesIdWithoutDuplicatesModulesId) &&
                in_array($question->module_id, $userModulesId)
            ) {
                array_push($modulesIdWithoutDuplicatesModulesId, $question->module_id);
            }
        }

        $modules = [];
        foreach ($modulesIdWithoutDuplicatesModulesId as $module_id) {
            array_push($modules, Module::findOrFail($module_id));
        }

        $questionsWithoutAnswerPerModule = [];
        foreach ($modules as $module_id) {
            array_push($questionsWithoutAnswerPerModule, $allQuestionsWithoutAnswer->whereIn('module_id', $module_id)->toArray());
        }
        $questions = $questionsWithoutAnswerPerModule;

        return view('home.teacher', compact('title', 'modules', 'questions'));
    }
}
