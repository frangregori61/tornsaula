<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\Question;
use App\Models\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    public function index()
    {
    }

    public function create($module_id)
    {
        $module = Module::findOrFail($module_id);
        $module_id = $module->id;
        $title = 'Create Unit for ' . strtoupper($module->initials);

        return view('units.create', compact('module_id', 'title'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'module_id' => 'required|integer|exists:modules,id',
            'unit' => 'required|integer|between:1,20',
            'short_description' => 'required|string|max:40',
        ]);

        $unit = Unit::create($validatedData);

        return redirect()->route('units.byModule', ['module_id' => $request['module_id']]);
    }

    public function show($id)
    {
    }

    public function edit($module_id, $unit_id)
    {
        $unit = Unit::findOrFail($unit_id);
        $module = Module::findOrFail($module_id);
        $unit_id = $unit->id;
        $title = strtoupper($module->initials) . ' Edit Unit ' . $unit->unit;
        return view('units.edit', compact('unit_id', 'title'));
    }

    public function update(Request $request, $unit_id)
    {
        $validatedData = $request->validate([
            'unit' => 'required|integer|between:1,20',
            'short_description' => 'required|string|max:40'
        ]);

        $unit = Unit::findOrFail($unit_id);
        $unit->update($validatedData);

        return redirect()->route('units.byModule', ['module_id' => $unit->module_id]);
    }

    public function destroy($id)
    {
        $unit = Unit::findOrFail($id);

        $unit->delete();
    }

    public function byModule($module_id)
    {
        $units = Unit::where('module_id', $module_id)->get();
        $module = Module::findOrFail($module_id);
        $title = strtoupper($module->initials);

        return view('units.byModule', compact('title', 'units', 'module_id'));
    }

    public function withQuestionsPendingToAnswerByModule($module_id)
    {
        $questionsWithoutAnswerPerModule = Question::doesntHave('answer')->where('module_id', $module_id)->get();

        $units = [];
        foreach ($questionsWithoutAnswerPerModule as $question) {
            if (!in_array($question->unit, $units))
                array_push($units, $question->unit);
        }

        $questions = [];
        foreach ($units as $unit) {
            array_push($questions, $questionsWithoutAnswerPerModule->where('unit_id', $unit->id));
        }
        $module = Module::findOrFail($module_id);
        $title = strtoupper($module->initials) . ' Questions to Answer';

        return view('units.withQuestionsPendingToAnswerByModule', compact('title', 'units', 'questions'));
    }

    public function byModuleWithMyQuestions($module_id)
    {
        $myQuestionsPerModule = Question::where('user_id', auth()->user()->id)->where('module_id', $module_id)->get();

        $unitsId = [];
        $units = [];
        foreach ($myQuestionsPerModule as $question) {
            if (!in_array($question->unit_id, $unitsId)) {
                array_push($unitsId, $question->unit_id);
                array_push($units, $question->unit);
            }
        }

        $questions = [];
        foreach ($units as $unit) {
            array_push($questions, $myQuestionsPerModule->where('unit_id', $unit->id));
        }
        $module = Module::findOrFail($module_id);

        $title = 'My Questions of ' . strtoupper($module->initials);

        return view('units.byModuleWithMyQuestions', compact('title', 'units', 'questions'));
    }
}
