<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\Question;
use App\Models\User;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function __invoke()
    {
        $questions = Question::count();
        $modules = Module::count();
        $users = User::all();

        if ($questions >= 2) {
            --$questions;
        }

        if ($modules >= 2) {
            --$modules;
        }

        $students = 0;
        $teachers = 0;

        foreach ($users as $user) {
            if ($user->role->type === 'STUDENT') {
                ++$students;
            }
            if ($user->role->type === 'TEACHER') {
                ++$teachers;
            }
        }

        if ($students >= 2) {
            --$students;
        }

        if ($teachers >= 2) {
            --$teachers;
        }

        return view('welcome', compact('questions', 'modules', 'students', 'teachers'));
    }
}
