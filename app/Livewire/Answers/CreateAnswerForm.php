<?php

namespace App\Livewire\Answers;

use App\Models\Answer;
use App\Models\Question;
use Livewire\Attributes\Validate;
use Livewire\Component;

class CreateAnswerForm extends Component
{
    #[Validate('required|integer|exists:questions,id')]
    public $question_id;

    #[Validate('required|integer|exists:users,id')]
    public $user_id;

    #[Validate('required|string|max:800')]
    public $content;

    public $question;

    public function mount()
    {
        $this->question = Question::findOrFail($this->question_id);
        $this->user_id = auth()->user()->id;
    }

    public function save()
    {
        $this->validate();

        $answer = Answer::create($this->only(['question_id', 'user_id', 'content']));

        return $this->redirect(route('questions.byModuleAndUnit', ['module_id' => $answer->question->module_id, 'unit_id' => $answer->question->unit_id]));
    }
    public function render()
    {
        return view('livewire.answers.create-answer-form');
    }
}
