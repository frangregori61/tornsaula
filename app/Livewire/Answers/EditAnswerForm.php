<?php

namespace App\Livewire\Answers;

use App\Models\Answer;
use App\Models\Question;
use Livewire\Attributes\Validate;
use Livewire\Component;

class EditAnswerForm extends Component
{
    #[Validate('required|string|max:800')]
    public $content;

    public $question_id;

    public $answer_id;

    public $question;

    public $answer;

    public function mount()
    {
        $this->question = Question::findOrFail($this->question_id);
        $this->answer = Answer::findOrFail($this->answer_id);

        $this->content = $this->answer->content;
    }

    public function save()
    {
        $this->validate();

        $this->answer->update($this->only(['content']));

        return $this->redirect(route('questions.byModuleAndUnit', ['module_id' => $this->answer->question->module_id, 'unit_id' => $this->answer->question->unit_id]));
    }
    public function render()
    {
        return view('livewire.answers.edit-answer-form');
    }
}
