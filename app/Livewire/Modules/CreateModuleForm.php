<?php

namespace App\Livewire\Modules;

use App\Models\Module;
use Livewire\Attributes\Validate;
use Livewire\Component;

class CreateModuleForm extends Component
{
    #[Validate('required|string|max:60')]
    public $name = '';

    #[Validate('required|string|max:9')]
    public $initials = '';

    #[Validate('required|string|max:120')]
    public $description = '';

    public function save()
    {
        $this->validate();

        Module::create($this->all());

        return $this->redirect(route('modules.index'));
    }

    public function render()
    {
        return view('livewire.modules.create-module-form');
    }
}
