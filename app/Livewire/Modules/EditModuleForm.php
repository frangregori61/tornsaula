<?php

namespace App\Livewire\Modules;

use App\Models\Module;
use Livewire\Attributes\Validate;
use Livewire\Component;

class EditModuleForm extends Component
{
    #[Validate('required|string|max:60')]
    public $name;

    #[Validate('required|string|max:9')]
    public $initials;

    #[Validate('required|string|max:120')]
    public $description;

    public $module_id;
    public $module;

    public function mount()
    {
        $this->module = Module::findOrFail($this->module_id);

        $this->name = $this->module->name;
        $this->initials = $this->module->initials;
        $this->description = $this->module->description;
    }

    public function save()
    {
        $this->validate();

        $this->module->update($this->only(['name', 'initials', 'description']));

        return $this->redirect(route('modules.index'));
    }

    public function render()
    {
        return view('livewire.modules.edit-module-form');
    }
}
