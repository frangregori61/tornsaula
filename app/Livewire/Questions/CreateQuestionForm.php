<?php

namespace App\Livewire\Questions;

use App\Models\Module;
use App\Models\Question;
use App\Models\Unit;
use Illuminate\Support\Facades\Storage;
use Livewire\Attributes\Validate;
use Livewire\Component;
use Livewire\WithFileUploads;


class CreateQuestionForm extends Component
{

    use WithFileUploads;

    #[Validate('required|integer|exists:users,id')]
    public $user_id;

    #[Validate('required|integer|exists:modules,id')]
    public $module_id;

    #[Validate('required|integer|exists:units,id')]
    public $unit_id;

    #[Validate('required|string|max:55')]
    public $title;

    #[Validate('required|string|max:800')]
    public $description;

    #[Validate('nullable|image|max:4096')]
    public $photo;

    public $unit;
    public $module;

    public function mount()
    {
        $this->user_id = auth()->user()->id;
        $this->unit = Unit::findOrFail($this->unit_id);
        $this->module = Module::findOrFail($this->module_id);
    }

    public function save()
    {
        $this->validate();

        if (!empty($this->photo)) {
            $path = Storage::disk('public')->putFile($this->photo);
            $this->photo = $path;
        }

        Question::create($this->only(['user_id', 'module_id', 'unit_id', 'title', 'description', 'photo']));

        return $this->redirect(route('questions.byModuleAndUnit', ['module_id' => $this->module_id, 'unit_id' => $this->unit_id]));
    }

    public function render()
    {
        return view('livewire.questions.create-question-form');
    }
}
