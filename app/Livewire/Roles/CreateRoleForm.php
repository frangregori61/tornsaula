<?php

namespace App\Livewire\Roles;

use App\Models\Role;
use Livewire\Attributes\Validate;
use Livewire\Component;

class CreateRoleForm extends Component
{
    #[Validate('required|string|max:30')]
    public $type = '';

    public function save()
    {
        $this->validate();

        Role::create($this->only(['type']));

        return $this->redirect(route('roles.index'));
    }
    public function render()
    {
        return view('livewire.roles.create-role-form');
    }
}
