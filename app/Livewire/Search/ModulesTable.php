<?php

namespace App\Livewire\Search;

use App\Models\Module;
use Livewire\Component;

class ModulesTable extends Component
{
    public $search;
    public $modules;
    public $items;

    public function mount()
    {
        $this->items = $this->modules;
    }

    public function updated()
    {
        if (empty($this->items)) {
            $this->items = $this->modules;
            return;
        }

        $this->items = Module::search($this->search)->get();
    }

    public function render()
    {
        return view('livewire.search.modules-table');
    }
}
