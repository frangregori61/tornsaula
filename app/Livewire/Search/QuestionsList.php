<?php

namespace App\Livewire\Search;

use App\Models\Question;
use Livewire\Component;

class QuestionsList extends Component
{
    public $search;
    public $user_id;
    public $module_id;
    public $unit_id;
    public $questions;
    public $items;

    public function mount()
    {
        $this->items = $this->questions;
    }

    public function updated()
    {
        if (empty($this->search)) {
            $this->items = $this->questions;
            return;
        }

        if (!empty($this->user_id) && !empty($this->module_id) && !empty($this->unit_id)) {
            $this->items = Question::search($this->search)->where('user_id', $this->user_id)->where('module_id', $this->module_id)
                ->where('unit_id', $this->unit_id)->get();
            return;
        }

        if (!empty($this->module_id) && !empty($this->unit_id)) {
            $this->items = Question::search($this->search)->where('module_id', $this->module_id)
                ->where('unit_id', $this->unit_id)->get();
            return;
        }
    }
    
    public function render()
    {
        return view('livewire.search.questions-list');
    }
}
