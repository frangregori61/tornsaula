<?php

namespace App\Livewire\Search;

use App\Models\Module;
use App\Models\User;
use Livewire\Component;

class UsersTable extends Component
{

    public $search;
    public $module_id;
    public $users;
    public $roles;
    public $items;

    public function mount()
    {
        $this->items = $this->users;
    }

    public function updated()
    {
        if (empty($this->search)) {
            $this->items = $this->users;
            return;
        }

        $users = User::search($this->search)->get();

        if (empty($this->module_id)) {
            $this->items = $users;
            return;
        }

        $this->items = $users->filter(function ($user) {
            if (in_array($this->module_id, array_column($user->modules->toArray(), 'id')))
                return $user;
        });
    }
    public function render()
    {
        return view('livewire.search.users-table');
    }
}
