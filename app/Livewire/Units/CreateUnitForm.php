<?php

namespace App\Livewire\Units;

use App\Models\Unit;
use Livewire\Attributes\Validate;
use Livewire\Component;

class CreateUnitForm extends Component
{
    #[Validate('required|integer|exists:modules,id')]
    public $module_id;

    #[Validate('required|integer|between:1,20')]
    public $unit;

    #[Validate('required|string|max:40')]
    public $short_description;

    public function save()
    {
        $this->validate();

        Unit::create($this->all());

        return $this->redirect(route('units.byModule', ['module_id' => $this->module_id]));
    }

    public function render()
    {
        return view('livewire.units.create-unit-form');
    }
}