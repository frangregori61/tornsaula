<?php

namespace App\Livewire\Units;

use App\Models\Unit;
use Livewire\Attributes\Validate;
use Livewire\Component;

class EditUnitForm extends Component
{
    public $unit_id;

    #[Validate('required|string|max:40')]
    public $short_description;

    #[Validate('required|integer|between:1,20')]
    public $unit;

    public $item;

    public function mount()
    {
        $this->item = Unit::findOrFail($this->unit_id);
        $this->unit = $this->item->unit;
        $this->short_description = $this->item->short_description;
    }

    public function save()
    {
        $this->validate();

        $this->item->update($this->only(['unit', 'short_description']));

        return $this->redirect(route('units.byModule', ['module_id' => $this->item->module_id]));
    }

    public function render()
    {
        return view('livewire.units.edit-unit-form');
    }
}
