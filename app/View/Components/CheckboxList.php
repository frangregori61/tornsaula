<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class CheckboxList extends Component
{

    public function __construct(public $data, public $name, public $modulesUser)
    {
    }

    public function render(): View|Closure|string
    {
        $data = $this->data;
        $name = $this->name;
        $modulesUser = $this->modulesUser;
        return view('components.checkbox-list', compact('data', 'modulesUser'));
    }
}
