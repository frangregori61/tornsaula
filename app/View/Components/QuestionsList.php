<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class QuestionsList extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(public $questions)
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        $questions = $this->questions;
        return view('components.questions-list', compact('questions'));
    }
}
