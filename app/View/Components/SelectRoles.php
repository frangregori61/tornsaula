<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class SelectRoles extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(public $name, public $id, public $roles, public $user)
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.select-roles');
    }
}
