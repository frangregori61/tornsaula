@if(empty($data))
    <p class="">No modules</p>
@else
    <div>
        @foreach ($data as $item )
            <div class="block">
                @if (in_array($item->id, $modulesUser))
                    <input type="checkbox" name={{$name}}[] id="modules" value={{$item->id}} class="rounded" checked>                    
                @else
                    <input type="checkbox" name={{$name}}[] id="modules" value={{$item->id}} class="rounded">                    
                @endif
                <label for="name" class="font-medium text-sm text-gray-700 dark:text-gray-300">{{$item->initials}}</label>
            </div>
        @endforeach
    </div>
@endif