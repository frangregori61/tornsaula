<input type="file"
{{$attributes->merge(['class' => 'block w-full text-sm text-gray-500
        file:me-4 file:py-2 file:px-4
        file:rounded-lg file:border-0
        file:text-sm file:font-semibold
        file:bg-orange-600 file:text-white
        hover:file:bg-orange-500
        hover:cursor-pointer
        file:hover:cursor-pointer
        file:disabled:opacity-50 file:disabled:pointer-events-none
        dark:text-neutral-500
        dark:file:bg-orange-600
        dark:hover:file:bg-orange-500'])}}
/>
