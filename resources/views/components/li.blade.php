<li {{$attributes->merge(['class' => 'bg-white dark:bg-neutral-800 overflow-hidden shadow-sm sm:rounded-lg text-gray-900 dark:text-gray-100 ' . $class])}}>
{{$slot}}
</li>