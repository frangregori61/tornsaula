@if (count($questions) === 0)
    <div class="bg-white dark:bg-neutral-800 overflow-hidden shadow-sm sm:rounded-lg mb-6">
        <div class="p-6 text-gray-900 dark:text-gray-100">
            <p>No hay preguntas</p>
        </div>
    </div>
@else
    <p class="text-center p-6 text-gray-900 dark:text-gray-100">Hay {{count($questions) === 1 ? count($questions) . ' pregunta' : count($questions) . ' preguntas'}}</p>

    <x-ul>
        @foreach ($questions as $question)
            <x-li class="p-9 mb-20">
                <div class="flex justify-between mb-6 text-lg font-light">
                    <small>{{$question->unit->module->initials}}</small>
                    <small>Unit {{$question->unit->unit}}</small>
                    <small>{{$question->user_id === auth()->user()->id ? 'Yo' : $question->user->name}}</small>
                </div>

                <div class="text-lg mb-12 px-2">
                    <h2 class="mb-2 font-bold">{{$question->title}}</h2>
                    <p>{{$question->description}}</p>
                    @if ($question->photo !== null)
                        <img src="{{asset('storage/' . $question->photo)}}" alt="photo" class="rounded-lg mt-4">
                    @endif
                </div>            

                <div class="flex flex-col text-lg">
                    @if (isset($question->answer))
                        <small class="mb-3 font-light">{{$question->answer->user->id == auth()->user()->id ? 'Yo' : $question->answer->user->name}}</small>
                        <p class="px-2">{{$question->answer->content}}</p>
                    @else
                        <p class="px-2">Sin contestar</p>
                    @endif
                </div>
                            
                @if ($question->user_id === auth()->user()->id)
                    <form method="POST" action="{{ route('questions.destroy', ['id' => $question->id]) }}" class="mt-14">
                        @csrf
                        @method('DELETE')
                        <x-delete-button type="submit" >{{ __('Delete') }}</x-delete-button>
                    </form>
                @endif

                @if(auth()->user()->role->type === 'TEACHER' && !isset($question->answer))
                    <div class="mt-14">
                        <x-a href="{{ route('answers.create', ['module_id' => $question->module->id, 'unit_id' => $question->unit->id, 'question_id' => $question->id]) }}">
                            <x-primary-button>{{ __('Answer') }}</x-primary-button>
                        </x-a>
                    </div>
                @endif

                @if(auth()->user()->role->type === 'TEACHER' && isset($question->answer))
                    <div class="mt-14">
                        <x-a href="{{ route('answers.edit', ['module_id' => $question->module_id, 'unit_id' => $question->unit->id, 'question_id' => $question->id, 'answer_id' => $question->answer->id]) }}" class="mt-6 space-y-6">
                            <x-primary-button>{{ __('Edit Answer') }}</x-primary-button>
                        </x-a>
                    </div>
                @endif
            </x-li>
        @endforeach
    </x-ul>
@endif