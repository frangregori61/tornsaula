<select name="{{$name}}" id="{{$id}}" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 sm:w-4/12 p-2.5 dark:bg-neutral-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
    @if (empty($user->role_id))
        <option value="" selected>-Escoge rol-</option>
    @endif
    @foreach ($roles as $role)
        @if ($role->id === $user->role->id)
            <option value="{{$role->id}}" selected>{{$role->type}}</option>                                                        
        @else
            <option value="{{$role->id}}">{{$role->type}}</option>
        @endif
    @endforeach
</select>