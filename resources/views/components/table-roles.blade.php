<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-neutral-700 dark:text-gray-400">
            <tr class="text-center">
                <th scope="col" class="px-6 py-3">
                    Role
                </th>
                <th scope="col" class="px-6 py-3">
                    Users
                </th>
                <th scope="col" class="px-6 py-3">
                    <span class="sr-only">Acctions</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($roles as $role)
                <tr class="bg-white border-b dark:bg-neutral-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-neutral-600 text-center">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        {{$role->type}}
                    </th>
                    <td class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        @if (count($role->users) == 0)
                            <p>Sin asignar</p>
                        @else
                            <x-a href="{{route('roles.users', ['id' => $role->id])}}" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">{{count($role->users)}} user/s</x-a>
                        @endif
                    </td>
                    <td class="px-6 py-4 items-center">
                        <form method="post" action="{{ route('roles.destroy', ['id' => $role->id]) }}">
                            @csrf
                            @method('DELETE')
                            <x-delete-button type="submit" >{{ __('Delete') }}</x-delete-button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
