<textarea placeholder="{{$placeholder}}" cols="{{$cols}}" rows="{{$rows}}"
{{$attributes->merge(
    ['class' => 'border-gray-300 dark:border-gray-700 dark:bg-neutral-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm w-full resize-none p-3 mt-1 ' . $class])}}>
{{$slot}}
</textarea>