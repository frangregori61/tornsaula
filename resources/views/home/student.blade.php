<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ $title }}
            </h2>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (count($modules) === 0)
                <div class="bg-white dark:bg-neutral-800 overflow-hidden shadow-sm sm:rounded-lg mb-6">
                    <div class="p-6 text-gray-900 dark:text-gray-100 flex justify-between">
                        <p>No has creado ninguna pregunta en ningún módulo :(</p>
                    </div>
                </div>
            @else
                <x-ul>
                    @foreach ($modules as $index => $value)
                        <x-li class="mb-12 w-full hover:bg-neutral-50 dark:hover:bg-neutral-700">
                            <x-a href="{{route('units.byModuleWithMyQuestions', ['module_id' => $value->id])}}" class="p-6 flex justify-between">
                                <h3>{{strtoupper($value->initials)}}</h3>
                                <strong>Has creado {{count($questions[$index]) === 1 ? count($questions[$index]) . ' pregunta' :
                                count($questions[$index]) . ' preguntas' }}</strong>
                            </x-a>
                        </x-li>
                    @endforeach
                </x-ul>
            @endif
        </div>
    </div>
</x-app-layout>