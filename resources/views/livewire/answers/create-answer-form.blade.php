<form wire:submit="save" class="p-6">
    @csrf

    <div class="flex justify-between mb-6 text-lg font-light">
        <small>{{$question->unit->module->initials}}</small>
        <small>Unit {{$question->unit->unit}}</small>
        <small>{{$question->user_id === auth()->user()->id ? 'Yo' : $question->user->name}}</small>
    </div>

    <div class="text-lg mb-12 px-2">
        <h2 class="mb-2 font-bold">{{$question->title}}</h2>
        <p>{{$question->description}}</p>
        @if ($question->photo !== null)
            <img src="{{asset('storage/' . $question->photo)}}" alt="photo" class="rounded-lg mt-4">
        @endif
    </div>

    <input type="hidden" wire:model="{{$question_id}}" value="{{$question_id}}">

    <div class="flex flex-col text-lg mb-6">
        <small class="mb-3 font-light">Yo</small>
        <x-textarea wire:model="content" placeholder="Answer..." />
        @error('content') <x-error-message message="{{$message}}" /> @enderror 
    </div>

    <x-save-button type="submit">{{ __('Save') }}</x-save-button>
</form>