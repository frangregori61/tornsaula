<form wire:submit="save" class="mt-6 space-y-6">
    @csrf

    <div>
        <x-input-label for="name" :value="__('Name')" />
        <x-input-text class="mt-1 block w-full" wire:model="name" />
        @error('name') <x-error-message message="{{$message}}" /> @enderror 
    </div>

    <div>
        <x-input-label for="initials" :value="__('Initials')" />
        <x-input-text class="mt-1 block w-full" wire:model="initials" />
        @error('initials') <x-error-message message="{{$message}}" /> @enderror 
    </div>

    <div>
        <x-input-label for="description" :value="__('Description')" />
        <x-input-text class="mt-1 block w-full" wire:model="description" />
        @error('description') <x-error-message message="{{$message}}" /> @enderror 
    </div>

    <x-save-button type="submit">{{ __('Save') }}</x-save-button>

</form>