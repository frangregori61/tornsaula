<form wire:submit="save" enctype="multipart/form-data">
    @csrf
    <div class="flex justify-between mb-6 text-lg font-light">
        <small>{{$module->initials}}</small>
        <small>Unit {{$unit->unit}}</small>
        <small>Yo</small>
    </div>
    
    <input type="hidden" wire:model="user_id" value="{{$user_id}}" />
    <input type="hidden" wire:model="unit_id" value="{{$unit_id}}" />
    <input type="hidden" wire:model="module_id" value="{{$module_id}}" />

    <div class="mb-6">
        <x-input-text wire:model="title" class="mt-1 block w-full" placeholder="Title..." />
        @error('title') <x-error-message message="{{$message}}" /> @enderror 
    </div>

    <div class="mb-6">
        <x-textarea wire:model="description" placeholder="Description..." rows="10" cols="10"></x-textarea>
        @error('description') <x-error-message message="{{$message}}" /> @enderror 
    </div>

    <div class="mb-12">
        <x-input-label for="photo" :value="__('Photo')" />
        <x-input-file wire:model="photo" />
        @error('photo') <x-error-message message="{{$message}}" /> @enderror 
    </div>

    <x-save-button type="submit">{{ __('Save') }}</x-save-button>

</form>