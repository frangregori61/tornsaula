<div>
    <x-input-text wire:model.live.debounce.400ms="search" placeholder="Search Modules..." class="mb-12" />

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-neutral-700 dark:text-gray-400">
                <tr class="text-center">
                    <th scope="col" class="px-6 py-3">
                        Name
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Initias
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Description
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Users
                    </th>
                    <th scope="col" class="px-6 py-3">
                        <span class="sr-only">Acctions</span>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($items as $module)
                    <tr class="bg-white border-b dark:bg-neutral-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-neutral-600 text-center">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{$module->name}}
                        </th>
                        <td class="px-6 py-4">
                            {{$module->initials}}
                        </td>
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{$module->description}}
                        </th>
                        <td class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            @if (count($module->users) == 0)
                                <p>Sin asignar</p>
                            @else
                                <x-a href="{{route('modules.usersByModule', ['id' => $module->id])}}" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">{{count($module->users)}} user/s</x-a>
                            @endif
                        </td>
                        <td class="flex py-4 space-x-5">               
                            <x-a href="{{route('modules.edit', ['module_id' => $module->id])}}" class="ml-6">
                                <x-edit-button type="submit">{{ __('Edit') }}</x-edit-button>
                            </x-a>
                            <form method="post" action="{{ route('modules.destroy', ['id' => $module->id]) }}">
                                @csrf
                                @method('DELETE')
                                <x-delete-button type="submit" >{{ __('Delete') }}</x-delete-button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>