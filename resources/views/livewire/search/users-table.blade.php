<div>
    <x-input-text wire:model.live.debounce.400ms="search" placeholder="Search Users..." class="mb-12" />
    @if (count($items) === 0)
        <p class="text-center p-6 text-gray-900 dark:text-gray-100">Hay {{count($items) === 1 ? count($items) . ' user' : count($items) . ' users'}}</p>
    @else
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-neutral-700 dark:text-gray-400">
                    <tr class="text-center">
                        <th scope="col" class="px-6 py-3">
                            Name
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Email
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Questions
                        </th>
                        @if(auth()->user()->role->type === 'ADMIN')
                            <th scope="col" class="px-6 py-3">
                                <span class="sr-only">Role</span>
                            </th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($items as $user)
                        <tr class="bg-white border-b dark:bg-neutral-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-neutral-600 text-center">
                            <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{$user->name}}
                            </th>
                            <td class="px-6 py-4">
                                {{$user->email}}
                            </td>
                            <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                @if(count($user->questions) >= 1)
                                    <x-a href="{{route('modules.byUser', ['user_id' => $user->id])}}" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">{{count($user->questions)}} question/s</x-a>
                                @else
                                    <p>Sin preguntas</p>
                                @endif
                            </th>
                            @if(auth()->user()->role->type === 'ADMIN')
                                <td class="px-6 py-4">
                                    <form method="post" action="{{ route('users.store.role', ['id' => $user->id]) }}" class="flex justify-around">
                                        @csrf
                                        @method('PATCH')

                                        <x-select-roles name="role_id" id="role_id" :roles="$roles" :user="$user" />
                                        <x-save-button type="submit">{{ __('Save') }}</x-save-button>
                                    </form>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>