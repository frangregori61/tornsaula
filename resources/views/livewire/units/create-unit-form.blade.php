<form  wire:submit="save" class="mt-6 space-y-6">
    @csrf

    <x-text-input id="module_id" name="module_id" type="hidden" class="mt-1 block w-full" :value="$module_id"/>

    <div>
        <x-input-label for="unit" :value="__('Unit')" />
        <x-input-text wire:model="unit" class="mt-1 block w-full"/>
        @error('unit') <x-error-message message="{{$message}}" /> @enderror 
    </div>

    <div>
        <x-input-label for="short_description" :value="__('Short description')" />
        <x-input-text wire:model="short_description" class="mt-1 block w-full"/>
        @error('short_description') <x-error-message message="{{$message}}" /> @enderror 
    </div>

    <x-save-button type="submit">{{ __('Save') }}</x-save-button>

</form>