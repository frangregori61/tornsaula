<form wire:submit="save"  class="mt-6 space-y-6">
    @csrf

    <div>
        <x-input-label for="unit" :value="__('Unit')" />
        <input type="number" wire:model="unit" class="mt-1 block w-full" />
        @error('unit') <x-error-message message="{{$message}}" /> @enderror 
    </div>

    <div>
        <x-input-label for="short_description" :value="__('Short description about unit')" />
        <x-input-text wire:model="short_description" class="mt-1 block w-full" />
        @error('short_description') <x-error-message message="{{$message}}" /> @enderror
    </div>

    <x-save-button type="submit">{{ __('Save') }}</x-save-button>

</form>