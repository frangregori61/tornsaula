<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ $title }}
            </h2>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (count($modules) === 0)
                <h2>No existen módulos</h2>
            @else
                <x-ul>
                    @foreach ($modules as $key => $value)
                        <x-li class="mb-12 hover:bg-neutral-50 dark:hover:bg-neutral-700">
                            <x-a href="{{route('questions.byUserAndModule', ['user_id' => $user->id, 'module_id' => $value->id])}}" class="p-6 flex justify-between w-full">
                                <p>{{$value->name}}</p>

                                @if ($questions[$key] === 0)
                                    <strong>Sin preguntas</strong>
                                @else
                                    <strong>{{$questions[$key] === 1 ? $questions[$key] . ' pregunta' : $questions[$key] . ' preguntas' }} </strong>
                                @endif    
                            </x-a>
                        </x-li>
                    @endforeach
                </x-ul>
            @endif
        </div>
    </div>
</x-app-layout>