<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ $title }}
            </h2>
            <x-a href="{{route('modules.create')}}">
                <x-primary-button>New Module</x-primary-button>
            </x-a>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (count($modules) === 0)
                <h2>No existen módulos</h2>
            @else
                <livewire:search.modules-table :modules="$modules" />
            @endif
        </div>
    </div>
</x-app-layout>