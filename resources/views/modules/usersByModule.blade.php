<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ $title }}
            </h2>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (empty($users))
                <h2>No existen users</h2>
            @else
                <livewire:search.users-table :users="$users" :roles="$roles" :module_id="$module_id" />
            @endif
        </div>
    </div>
</x-app-layout>