<div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
    <x-nav-link :href="route('home.admin')" :active="request()->routeIs('home.admin')">
        {{ __('Home') }}
    </x-nav-link>
    <x-nav-link :href="route('roles.index')" :active="request()->routeIs('roles.index')">
        {{ __('Roles') }}
    </x-nav-link>
    <x-nav-link :href="route('users.index')" :active="request()->routeIs('users.index')">
        {{ __('Users') }}
    </x-nav-link>
    <x-nav-link :href="route('modules.index')" :active="request()->routeIs('modules.index')">
        {{ __('Modules') }}
    </x-nav-link>
</div>