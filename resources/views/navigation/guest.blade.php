<div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
    <x-nav-link :href="route('home.guest')" :active="request()->routeIs('home.guest')">
        {{ __('Home') }}
    </x-nav-link>
</div>