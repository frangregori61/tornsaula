<div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
    <x-nav-link :href="route('home.teacher')" :active="request()->routeIs('home.teacher')">
        Home
    </x-nav-link>

    @foreach (Auth::user()->modules as $module)
        <x-nav-link :href="route('units.byModule', ['module_id' => $module->id])"
            :active="url()->current() === route('units.byModule', $module->id)">
            {{ strtoupper($module->initials) }}
        </x-nav-link>
    @endforeach
</div>