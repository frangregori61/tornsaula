<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900 dark:text-gray-100">
            {{ __('Modules Selected') }}
        </h2>

        <p class="mt-1 text-sm text-gray-600 dark:text-gray-400">
            {{ __("Choose your modules.") }}
        </p>
    </header>

    <form method="post" action="{{ route('modules.storeModulesOfUser') }}" class="mt-6 space-y-6">
        @csrf
        @method('post')

        <x-checkbox-list :data="$data" name="modules" :modulesUser="$modulesUser"/>
        
        <x-save-button type="submit">{{ __('Save') }}</x-save-button>
    </form>
</section>
