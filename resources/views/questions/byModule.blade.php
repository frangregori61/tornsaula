<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ $title }}
            </h2>

            @if (Auth::user()->role->type === 'TEACHER')
                <x-nav-link :href="route('units.create', ['module_id' => $module_id])">
                    {{ __('New Unit') }}
                </x-nav-link>
            @endif
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-ul>
                @foreach ($questions as $question)
                    <x-li class="mb-12 flex justify-between p-6">
                        <h2>{{$question->title}}</h2>
                        <p>{{$question->description}}</p>
                    <x-li class="mb-12">
                @endforeach
            </x-ul>
        </div>
    </div>
</x-app-layout>