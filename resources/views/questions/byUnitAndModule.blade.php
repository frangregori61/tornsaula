<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ $title }}
            </h2>

            @if (Auth::user()->role->type === 'STUDENT')
                <x-a href="{{route('questions.create', ['module_id' => $unit->module->id ,'unit_id' => $unit->id])}}">
                    <x-primary-button>New Question</x-primary-button>
                </x-a>
            @endif
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <livewire:search.questions-list :questions="$questions" :module_id="$module_id" :unit_id="$unit_id" />
        </div>
    </div>
</x-app-layout>