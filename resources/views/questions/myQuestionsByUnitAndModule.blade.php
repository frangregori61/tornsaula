<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ $title }}
            </h2>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <livewire:search.questions-list :questions="$questions" :user_id="$user_id" :module_id="$module_id" :unit_id="$unit_id" />
        </div>
    </div>
</x-app-layout>