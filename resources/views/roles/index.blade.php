<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ $title }}
            </h2>
            <x-a href="{{route('roles.create')}}">
                <x-primary-button>New Role</x-primary-button>
            </x-a>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (empty($roles))
                <h2>No existen roles</h2>
            @else
                <x-table-roles :roles="$roles" />
            @endif
        </div>
    </div>
</x-app-layout>