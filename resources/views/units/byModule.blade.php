<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ $title }}
            </h2>

            @if (Auth::user()->role->type === 'TEACHER')
                <x-a href="{{route('units.create', ['module_id' => $module_id])}}">
                    <x-primary-button type="button">New Unit</x-primary-button>
                </x-a>
            @endif
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if(auth()->user()->role->type === 'TEACHER')
                <x-a href="{{route('users.byModule', ['module_id' => $module_id])}}" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">View user/s</x-a>
            @endif
            
            @if (count($units) === 0)
                <div class="bg-white dark:bg-neutral-800 overflow-hidden shadow-sm sm:rounded-lg mb-6">
                    <div class="p-6 text-gray-900 dark:text-gray-100">
                        <p>No hay unidades</p>
                    </div>
                </div>
            @else
                <x-ul>
                    @foreach ($units as $unit)
                        <x-li class="mb-12 flex items-center hover:bg-neutral-50 dark:hover:bg-neutral-700">
                            @if (auth()->user()->role->type === 'TEACHER')
                                <x-a href="{{route('units.edit', ['module_id' => $module_id, 'unit_id' => $unit->id])}}" class="ml-6">
                                    <x-edit-button type="submit">{{ __('Edit') }}</x-edit-button>
                                </x-a>
                            @endif
                            
                            <x-a href="{{route('questions.byModuleAndUnit', ['module_id' => $module_id, 'unit_id' => $unit->id])}}" class="p-6 flex justify-between w-full" >
                                <p>Unit {{$unit->unit}}</p>

                                <p>{{ucfirst($unit->short_description)}}</p>

                                @if (count($unit->questions) === 0)
                                    <strong>Sin preguntas</strong>
                                @else
                                    <strong>{{count($unit->questions) === 1 ? count($unit->questions) . ' pregunta' : count($unit->questions) . ' preguntas' }} </strong>
                                @endif    
                            </x-a>
                        </x-li>
                    @endforeach
                </x-ul>
            @endif
        </div>
    </div>
</x-app-layout>