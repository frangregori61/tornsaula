<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ $title }}
            </h2>

            @if (Auth::user()->role->type === 'TEACHER')
                <x-nav-link :href="route('units.create', ['module_id' => $module_id])">
                    {{ __('New Unit') }}
                </x-nav-link>
            @endif
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-ul>
                @foreach ($units as $key => $value)
                    <x-li class="mb-12 w-full hover:bg-neutral-50 dark:hover:bg-neutral-700">
                        <x-a href="{{route('questions.myQuestionsByUnitAndModule', ['module_id' => $value->module->id ,'unit_id' => $value->id])}}" class="p-6 flex justify-between">
                            <p>Unit {{$value->unit}}</p>

                            <p>{{ucfirst($value->short_description)}}</p>

                            @if (count($questions[$key]) === 0)
                                <strong>Sin preguntas</strong>
                            @else
                                <strong>Has creado {{count($questions[$key]) === 1 ? count($questions[$key]) . ' pregunta' : count($questions[$key]) . ' preguntas' }}</strong>
                            @endif    
                        </x-a>
                    </x-li>
                @endforeach
            </x-ul>
        </div>
    </div>
</x-app-layout>