<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ $title }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">        
            <x-ul>
                @foreach ($units as $key => $value)
                    <x-li class="mb-12 hover:bg-neutral-50 dark:hover:bg-neutral-700">
                        <x-a href="{{route('questions.pendingToAnswerByModuleAndUnit', ['module_id' => $value->module_id, 'unit_id' => $value->id])}}" class="p-6 flex justify-between">
                            <p>Unit {{$value->unit}}</p>

                            <p>{{ucfirst($value->short_description)}}</p>
                            
                            <strong>{{count($questions[$key]) === 1 ? count($questions[$key]) . ' pregunta' : count($questions[$key]) . ' preguntas' }} por contestar</strong>
                        </x-a>
                    </x-li>
                @endforeach
            </x-ul>
        </div>
    </div>
</x-app-layout>