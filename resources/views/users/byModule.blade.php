<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ $title }}
            </h2>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-neutral-700 dark:text-gray-400">
                        <tr class="text-center">
                            <th scope="col" class="px-6 py-3">
                                Name
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Email
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Questions
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $key => $value)
                            <tr class="bg-white border-b dark:bg-neutral-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-neutral-600 text-center">
                                <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    {{$value->name}}
                                </th>
                                <td class="px-6 py-4">
                                    {{$value->email}}
                                </td>
                                <td scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    @if ($questions[$key] === 0)
                                        <p>Sin preguntas</p>
                                    @else
                                        <a href="{{route('questions.byUserAndModule', ['user_id' => $value->id, 'module_id' => $module_id])}}" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">{{$questions[$key]}} question/s</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>