<?php

use App\Http\Controllers\AdminHomeController;
use App\Http\Controllers\AnswerController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\GuestHomeController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StudentHomeController;
use App\Http\Controllers\TeacherHomeController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', WelcomeController::class)->name('welcome');

Route::get('/home/student', StudentHomeController::class)->middleware(['auth', 'verified'])->name('home.student');
Route::get('/home/teacher', TeacherHomeController::class)->middleware(['auth', 'verified'])->name('home.teacher');
Route::get('/home/admin', AdminHomeController::class)->middleware(['auth', 'verified'])->name('home.admin');
Route::get('/home/guest', GuestHomeController::class)->middleware(['auth', 'verified'])->name('home.guest');


Route::get('/roles', [RoleController::class, 'index'])->name('roles.index');
Route::get('/roles/create', [RoleController::class, 'create'])->name('roles.create');
Route::post('/roles', [RoleController::class, 'store'])->name('roles.store');
Route::get('/roles/{id}', [RoleController::class, 'show'])->name('roles.show');
Route::get('/roles/{id}/edit', [RoleController::class, 'edit'])->name('roles.edit');
Route::patch('/roles/{id}', [RoleController::class, 'update'])->name('roles.update');
Route::delete('/roles/{id}/delete', [RoleController::class, 'destroy'])->name('roles.destroy');
Route::get('/role/{id}/users', [RoleController::class, 'usersByRole'])->name('roles.users');


Route::get('/modules', [ModuleController::class, 'index'])->name('modules.index');
Route::get('/modules/create', [ModuleController::class, 'create'])->name('modules.create');
Route::post('/modules', [ModuleController::class, 'store'])->name('modules.store');
Route::get('/modules/{id}', [ModuleController::class, 'show'])->name('modules.show');
Route::get('/modules/{module_id}/edit', [ModuleController::class, 'edit'])->name('modules.edit');
Route::patch('/modules/{id}', [ModuleController::class, 'update'])->name('modules.update');
Route::delete('/module/{id}/delete', [ModuleController::class, 'destroy'])->name('modules.destroy');
Route::get('/module/{id}/users', [ModuleController::class, 'usersByModule'])->name('modules.usersByModule');
Route::post('/modules/add/user', [ModuleController::class, 'storeModulesOfUser'])->name('modules.storeModulesOfUser');
Route::get('/modules-with-questions-pending-to-answer', [ModuleController::class, 'modulesWithQuestionsPendingToAnswer'])->name('modules.withQuestionsPendingToAnswer');
Route::get('/user/{user_id}/modules', [ModuleController::class, 'byUser'])->name('modules.byUser');


Route::get('/units', [UnitController::class, 'index'])->name('units.index');
Route::get('/module/{module_id}/unit/create', [UnitController::class, 'create'])->name('units.create');
Route::post('/units', [UnitController::class, 'store'])->name('units.store');
Route::get('/units/{id}', [UnitController::class, 'show'])->name('units.show');
Route::get('/module/{module_id}/unit/{unit_id}/edit', [UnitController::class, 'edit'])->name('units.edit');
Route::patch('/units/{unit_id}', [UnitController::class, 'update'])->name('units.update');
Route::get('/units/{id}/delete', [UnitController::class, 'destroy'])->name('units.destroy');
Route::get('/module/{module_id}/units', [UnitController::class, 'byModule'])->name('units.byModule');
Route::get('/module/{module_id}/questions-pending-to-answer', [UnitController::class, 'withQuestionsPendingToAnswerByModule'])->name('units.withQuestionsPendingToAnswerByModule');
Route::get('/home/module/{module_id}/units', [UnitController::class, 'byModuleWithMyQuestions'])->name('units.byModuleWithMyQuestions');

Route::get('/questions', [QuestionController::class, 'index'])->name('questions.index');
// revisar ese get, mejor poner post
Route::get('/module/{module_id}/unit/{unit_id}/create/question', [QuestionController::class, 'create'])->name('questions.create');
Route::post('/questions', [QuestionController::class, 'store'])->name('questions.store');
Route::get('/questions/{id}', [QuestionController::class, 'show'])->name('questions.show');
Route::get('/questions/{id}/edit', [QuestionController::class, 'edit'])->name('questions.edit');
Route::patch('/questions/{id}', [QuestionController::class, 'update'])->name('questions.update');
Route::delete('/question/{id}/delete', [QuestionController::class, 'destroy'])->name('questions.destroy');
Route::get('/my-questions', [QuestionController::class, 'myQuestions'])->name('questions.myQuestions');
Route::get('/module/{module_id}/questions', [QuestionController::class, 'byModule'])->name('questions.byModule');
Route::get('/module/{module_id}/unit/{unit_id}/questions', [QuestionController::class, 'byModuleAndUnit'])->name('questions.byModuleAndUnit');
Route::get('/module/{module_id}/unit/{unit_id}/questions-pending-to-answer', [QuestionController::class, 'pendingToAnswerByModuleAndUnit'])->name('questions.pendingToAnswerByModuleAndUnit');
Route::get('/module/{module_id}/unit/{unit_id}/my-questions', [QuestionController::class, 'myQuestionsByUnitAndModule'])->name('questions.myQuestionsByUnitAndModule');
Route::get('/user/{user_id}/module/{module_id}/questions', [QuestionController::class, 'byUserAndModule'])->name('questions.byUserAndModule');
// revisar
Route::get('/answers', [AnswerController::class, 'index'])->name('answers.index');
// revisar ese get, mejor post
Route::get('/module/{module_id}/unit/{unit_id}/question/{question_id}/answer', [AnswerController::class, 'create'])->name('answers.create');
Route::post('/answers', [AnswerController::class, 'store'])->name('answers.store');
Route::get('/answers/{id}', [AnswerController::class, 'show'])->name('answers.show');
// revisar lo del filtrado
Route::get('/module/{module_id}/unit/{unit_id}/question/{question_id}/answer/{answer_id}/edit', [AnswerController::class, 'edit'])->name('answers.edit');
Route::patch('/answers/{id}', [AnswerController::class, 'update'])->name('answers.update');
Route::delete('/answers/{id}/delete', [AnswerController::class, 'destroy'])->name('answers.destroy');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});


Route::get('/users', [RegisteredUserController::class, 'index'])->name('users.index');
Route::patch('/users/{id}/add-role', [RegisteredUserController::class, 'userStoreRole'])->name('users.store.role');
Route::get('/module/{module_id}/users', [RegisteredUserController::class, 'byModule'])->name('users.byModule');

require __DIR__ . '/auth.php';